// NPM Imports
import { error, log, task, warn } from './logger'
import { src, dest, watch, series, parallel } from 'gulp'

// Config
import { WATCH_SASS, WATCH_JS } from './config'

// Tasks
import compileSass from './tasks/compileSass'
import compileJs from './tasks/compileJs'

export const watchSass = async () => {
  await compileSass()
  task('SASS', 'Compiled CSS')

  task('SASS', 'Watching files for changes...')
  return watch(WATCH_SASS, async done => {
    task('SASS', 'Recompiling CSS...', false)
    await compileSass()
    log(' DONE!')
    done()
  })
}

export const watchJs = async () => {
  await compileJs()
  task('BABEL', 'Compiled JS')

  task('BABEL', 'Watching files for changes...')
  return watch(WATCH_JS, async done => {
    task('BABEL', 'Recompiling JS...', false)
    await compileJs()
    log(' DONE!')
    done()
  })
}

export default parallel([ watchSass, watchJs ])