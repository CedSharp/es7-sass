import path from 'path'

export const ROOT = path.resolve(__dirname, '..')

// -- SASS --------------------------------------------------------------------------

export const CSS_DIR = path.join(ROOT, 'assets', 'css')

export const SASS_DIR = path.join(ROOT, 'src', 'sass')
export const SASS_FILE = path.join(SASS_DIR, 'styles.sass')

export const WATCH_SASS = [
  path.join(SASS_DIR, '**', '*.sass'),
  path.join(SASS_DIR, '**', '*.scss')
]

// -- JS ----------------------------------------------------------------------------

export const JS_SRC_DIR = path.join(ROOT, 'src', 'js')
export const JS_SRC_FILE = path.join(JS_SRC_DIR, 'app.js')

export const JS_DEST_DIR = path.join(ROOT, 'assets', 'js')
export const JS_DEST_FILE = 'script.js'

export const WATCH_JS = [ path.join(JS_SRC_DIR, '**', '*.js') ]