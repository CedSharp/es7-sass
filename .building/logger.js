const c = require('chalk')

const log = (desc, newline=true) => newline ? console.log(desc) : process.stdout.write(desc)
const task = (name, desc, newline=true) => log(c.cyan(`[ ${name} ]`)+`: ${desc}`, newline)
const error = (desc, newline) => log(c.red('[ ERROR ]')+`: ${desc}`, newline)
const warn = (desc, newline) => log(c.yellow('[ WARN ]')+`: ${desc}`, newline)

module.exports = { log, task, error, warn }