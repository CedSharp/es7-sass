import { src, dest } from 'gulp'
import rename from 'gulp-rename'
import { SASS_FILE, CSS_DIR } from '../config'

import { default as sass } from 'gulp-sass'
import { default as sassCompiler } from 'node-sass'
sass.compiler = sassCompiler

const compileSass = () => new Promise((resolve, reject) => {
  return src(SASS_FILE)
    .pipe(sass().on('error', sass.logError))
    .on('error', reject)
    .pipe(rename('styles.css'))
    .pipe(dest(CSS_DIR))
    .on('end', resolve)
})

export default compileSass